package data;

import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import pages.CommonUtils;

public class UserDataProvider {
    static Logger log = Logger.getLogger(Log.class.getName());

    public static UserData test_user_for_sign_up() {
        String userName = CommonUtils.randomAlpha(7).toLowerCase() + "@yopmail.com";
        log.info(userName);
        return UserData.UserDataBuilder.userData()
                .withUserName(userName)
                .withUserEmail(userName)
                .withUserPassword("123hardpassword123")
                .build();
    }
}
