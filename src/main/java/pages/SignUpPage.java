package pages;

import data.UserData;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignUpPage extends BasePage {
    @FindBy(id = "name")
    private WebElement name_field;
    @FindBy(id = "email")
    private WebElement email_input;
    @FindBy(id = "password")
    private WebElement password_input;
    @FindBy(id = "confirmationPassword")
    private WebElement confirmationPassword;
    @FindBy(xpath = "//button[contains(@type,'submit')]")
    private WebElement signUp;

    public SignUpPage() {
    }

    public void typeName(String name) {
        type(name_field, name);
    }

    public void typeEmail(String email) {
        type(email_input, email);
    }

    public void typePassword(String password) {
        type(password_input, password);
    }

    public void typeConfirmationPassword(String password) {
        type(confirmationPassword, password);
    }

    public void clickSignUp() {
        click(signUp);
    }

    public void signUp(UserData userData) {
        typeEmail(userData.getUserEmail());
        typeName(userData.getUsername());
        typePassword(userData.getUserPassword());
        typeConfirmationPassword(userData.getUserPassword());
        clickSignUp();
    }

    public void signUp(String email, String name, String password) {
        typeEmail(email);
        typeName(name);
        typePassword(password);
        typeConfirmationPassword(password);
        clickSignUp();
    }

    public void visitSignUpPage() {
        visit(getUrl());
    }

    @Override
    public String getUrl() {
        return BASE_URL + "/register";
    }
}
