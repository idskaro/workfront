import data.UserData;
import data.UserDataProvider;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.DashboardPage;
import pages.HomePage;
import pages.SignInPage;
import pages.SignUpPage;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SignInTest extends BaseTest {
    UserData userData;
    HomePage homePage;
    SignInPage signInPage;
    DashboardPage dashboardPage;
    SignUpPage signUpPage;

    @BeforeMethod
    public void setUp() throws InterruptedException {
        dashboardPage = new DashboardPage();
        homePage = new HomePage();
        signInPage = new SignInPage();
        userData = new UserData();
        signUpPage = new SignUpPage();
    }

    @Test
    //TODO case1
    public void signInWithValidCredentials() {
        userData = UserDataProvider.test_user_for_sign_up();
        signUpPage.visitSignUpPage();
        signUpPage.signUp(userData);
        dashboardPage.logOut();
        signInPage.visitSignInPage();
        signInPage.signIn(userData.getUserEmail(), userData.getUserPassword());
        assertTrue(dashboardPage.isUserLogedIn(), "User sign in - FAIL");
    }

    @Test
    //TODO case2
    public void signInWithInvalidPassword() {
        userData = UserDataProvider.test_user_for_sign_up();
        signUpPage.visitSignUpPage();
        signUpPage.signUp(userData);
        dashboardPage.logOut();
        signInPage.visitSignInPage();
        signInPage.signIn(userData.getUserEmail(), "invalid");
        assertFalse(dashboardPage.isUserLogedIn(), "User sign in FAILED");
        assertTrue(signInPage.invalidCredentialsNoticeAppears(), "Invalid credentials notice does not displayed");

    }

    @Test
    //TODO case3
    public void signInWithInvalidEmail() {
        userData = UserDataProvider.test_user_for_sign_up();
        signInPage.visitSignInPage();
        signInPage.typeEmail("test");
        signInPage.signIn("abc", "qweqwe");
        assertFalse(dashboardPage.isUserLogedIn(), "User sign in FAILED");
        assertTrue(signInPage.loginFailedNoticeAppears(), "Login failed notice does not appears");
        assertTrue(signInPage.invalidEmailNoticeAppears(), "Invalid email notice does not appears");
    }
}
