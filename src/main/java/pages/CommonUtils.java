package pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import static setup.DriverSetup.getDriver;

public abstract class CommonUtils extends BasePage {

    public CommonUtils() {
        driver = getDriver();
    }

    public static String randomAlpha(int length) {
        String alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String randomString = generateRandomString(length, alpha);
        return randomString;
    }

    public static String generateRandomString(int length, String chars) {
        String randomStr = "";
        Random r = new Random();
        for (int i = 0; i < length; i++) {
            randomStr += chars.charAt(r.nextInt(chars.length()));
        }
        return randomStr;
    }

    public static String getDueDate(String dueDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        if (dueDate.equals("today")) {
            Date date = calendar.getTime();
            return dateFormat.format(date).toString();
        }
        if (dueDate.equals("yesterday")) {
            calendar.add(Calendar.DATE, -1);
            Date date = calendar.getTime();
            return dateFormat.format(date).toString();
        }
        if (dueDate.equals("tomorrow")) {
            calendar.add(Calendar.DATE, +1);
            Date date = calendar.getTime();
            return dateFormat.format(date).toString();
        } else return null;
    }


}