package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {
    @FindBy(xpath = "//a[contains(.,'Sign in')]")
    private WebElement signIn;
    @FindBy(xpath = "//a[contains(.,'Sign up')]")
    private WebElement signUp;

    public HomePage() {
    }

    public void clickSignIn() {
        click(signIn);
    }

    public void clickSignUp() {
        click(signUp);
    }

    public void visitHomePage() {
        visit(getUrl());
    }

    @Override
    public String getUrl() {
        return BASE_URL;
    }
}
