package pages;

import data.UserData;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

    UserData userData;

    @FindBy(id = "email")
    private WebElement email_input;
    @FindBy(id = "password")
    private WebElement password_input;
    @FindBy(xpath = "//button[contains(@type,'submit')]")
    private WebElement signIn;
    @FindBy(xpath = "//a[contains(.,'Register here for free!')]")
    private WebElement register;
    @FindBy(xpath = "//strong[contains(.,'Login Error: invalid email/password')]")
    private WebElement invalidCredentialsNotice;
    @FindBy(xpath = "//strong[contains(.,'Login failed! please fix the following errors:')]")
    private WebElement loginFailedNotice;
    @FindBy(id = "email.errors")
    private WebElement invalidFormatEmailNotice;

    public SignInPage() {
        userData = new UserData();
    }

    public void typeEmail(String email) {
        type(email_input, email);
    }

    public void typePassword(String password) {
        type(password_input, password);
    }

    public void clickSignIn() {
        click(signIn);
    }

    public void clickSignUp() {
        click(register);
    }

    public void signIn(String email, String password) {
        typeEmail(email);
        typePassword(password);
        clickSignIn();
    }

    public boolean invalidCredentialsNoticeAppears() {
        return invalidCredentialsNotice.isDisplayed();
    }

    public boolean invalidEmailNoticeAppears() {
        return invalidFormatEmailNotice.isDisplayed();
    }

    public boolean loginFailedNoticeAppears() {
        return loginFailedNotice.isDisplayed();
    }

    public void visitSignInPage() {
        visit(getUrl());
    }

    @Override
    public String getUrl() {
        return BASE_URL + "/login";
    }
}
