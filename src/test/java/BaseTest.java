import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import setup.listeners.SuiteListener;

import static setup.DriverSetup.initDriver;
import static setup.DriverSetup.quitDriver;

@Listeners(SuiteListener.class)
public class BaseTest {
    @BeforeMethod
    public void setUpBase() {
        initDriver();
    }

    @AfterMethod
    public void tearDownBase() {
        quitDriver();
    }
}
