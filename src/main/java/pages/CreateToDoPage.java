package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class CreateToDoPage extends BasePage {
    public String low = "low";
    public String medium = "medium";
    public String high = "high";
    @FindBy(id = "title")
    private WebElement title;
    @FindBy(id = "dueDate")
    private WebElement dueDate;
    @FindBy(id = "priority")
    private WebElement priority;
    @FindBy(xpath = "//button[contains(.,'Create')]")
    private WebElement create;
    @FindBy(xpath = "//button[contains(.,'Cancel')]")
    private WebElement cancel;
    @FindBy(xpath = "//a[contains(.,' Delete')]")
    private WebElement delete;

    public CreateToDoPage() {
    }

    public void typeTitle(String email) {
        type(title, email);
    }

    public void typeDueDate(String value) {
        clear(this.dueDate);
        type(dueDate, CommonUtils.getDueDate(value));
    }

    public void selectPriority(String value) {
        Select priority_dropdown = new Select(priority);
        if (value == low) {
            priority_dropdown.selectByValue("LOW");
        }
        if (value == medium) {
            priority_dropdown.selectByValue("MEDIUM");
        }
        if (value == high) {
            priority_dropdown.selectByValue("HIGH");
        }
    }

    public void clickCreate() {
        click(create);
    }

    public void clickCancel() {
        click(cancel);
    }

    public void visitCreateToDoPage() {
        visit(getUrl());
    }

    public boolean createToDoPageIsOpened() {
        return getCurrentURL().endsWith("/user/todos/new");
    }

    public boolean deleteButtonIsDisplayed() {
        try {
            delete.isDisplayed();
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    @Override
    public String getUrl() {
        return BASE_URL + "/user/todos/new";
    }
}
