package setup;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;

import static org.openqa.selenium.remote.BrowserType.CHROME;
import static org.openqa.selenium.remote.BrowserType.FIREFOX;

public class DriverSetup {
    public static String BROWSER =
            System.getProperty("selenium.browser", CHROME);
    private static WebDriver driver;
    private static ThreadLocal<WebDriver> driverThread = new ThreadLocal<>();

    private static String getDriverPath(Platform platform, String browserType) {
        String fs = File.separator;
        String driverPath = System.getProperty("user.dir") + fs + "src" + fs + "main" + fs + "resources" + fs + "drivers" + fs;
        switch (platform) {
            case MAC:
                driverPath += "mac" + fs;
                break;
            case WIN10:
                driverPath += "win" + fs;
                break;
            case LINUX:
                driverPath += "linux" + fs;
                break;
        }
        switch (browserType) {
            case CHROME:
                driverPath += "chromedriver.exe";
                break;
            case FIREFOX:
                driverPath = "geckodriver.exe";
                break;
        }
        return driverPath;
    }

    public static void initDriver() {
        if (driver == null) {
            switch (BROWSER) {
                case "chrome":
                    String chromeDriverLocation = getDriverPath(Platform.getCurrent(), BROWSER);
                    System.setProperty("webdriver.chrome.driver", chromeDriverLocation);
                    driverThread.set(new ChromeDriver());
                    break;
                case "firefox":
                    String firefoxDriverLocation = getDriverPath(Platform.getCurrent(), BROWSER);
                    System.setProperty("webdriver.gecko.driver", firefoxDriverLocation);
                    driver = new FirefoxDriver();
                    break;
            }
        }
    }

    public static WebDriver getDriver() {
        return driverThread.get();
    }

    public static void quitDriver() {
        getDriver().close();
        getDriver().quit();
    }
}
