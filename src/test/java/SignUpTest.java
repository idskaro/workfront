import data.UserData;
import data.UserDataProvider;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.DashboardPage;
import pages.HomePage;
import pages.SignUpPage;

import static org.testng.Assert.assertTrue;

public class SignUpTest extends BaseTest {
    UserData userData;
    HomePage homePage;
    DashboardPage dashboardPage;
    SignUpPage signUpPage;

    @BeforeMethod
    public void setUp() throws InterruptedException {
        userData = new UserData();
        dashboardPage = new DashboardPage();
        homePage = new HomePage();
        signUpPage = new SignUpPage();
        signUpPage.visitSignUpPage();
    }

    @Test
    public void verifySignUpWithValidCredentials() {
        userData = UserDataProvider.test_user_for_sign_up();
        signUpPage.signUp(userData);
        assertTrue(dashboardPage.isUserLogedIn(), "User sign up - FAIL");
    }
}
