import data.UserData;
import data.UserDataProvider;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.*;

import static org.testng.Assert.*;

public class CreateToDoTest extends BaseTest {
    UserData userData;
    HomePage homePage;
    SignInPage signInPage;
    DashboardPage dashboardPage;
    SignUpPage signUpPage;
    CreateToDoPage createToDoPage;
    String title = CommonUtils.randomAlpha(5);
    String today = CommonUtils.getDueDate("today");
    String yesterday = CommonUtils.getDueDate("yesterday");
    String tomorrow = CommonUtils.getDueDate("tomorrow");
    String low = "low";
    String medium = "medium";
    String high = "high";

    @BeforeMethod
    public void setUp() throws InterruptedException {
        dashboardPage = new DashboardPage();
        homePage = new HomePage();
        signInPage = new SignInPage();
        userData = new UserData();
        signUpPage = new SignUpPage();
        createToDoPage = new CreateToDoPage();
        signUpPage.visitSignUpPage();
        userData = UserDataProvider.test_user_for_sign_up();
        signUpPage.signUp(userData);
        dashboardPage.clickCreateToDo();
    }

    @Test
    //TODO case4
    public void createToDoWithValidCredentialsTodayLow() {
        createToDoPage.typeDueDate("today");
        createToDoPage.typeTitle(title);
        createToDoPage.selectPriority(low);
        createToDoPage.clickCreate();
        assertEquals(title, dashboardPage.getTitle());
        assertEquals(today, dashboardPage.getDueDate());
        assertEquals(low, dashboardPage.getPrio());
    }

    @Test
    //TODO case5
    public void createToDoWithValidCredentialYesterdayMedium() {
        createToDoPage.typeDueDate("yesterday");
        createToDoPage.typeTitle(title);
        createToDoPage.selectPriority(medium);
        createToDoPage.clickCreate();
        assertEquals(title, dashboardPage.getTitle());
        assertEquals(yesterday, dashboardPage.getDueDate());
        assertEquals(medium, dashboardPage.getPrio());
    }

    @Test
    //TODO case6
    public void createToDoWithValidCredentialsTomorrowHigh() {
        createToDoPage.typeDueDate("tomorrow");
        createToDoPage.typeTitle(title);
        createToDoPage.selectPriority(high);
        createToDoPage.clickCreate();
        assertEquals(title, dashboardPage.getTitle());
        assertEquals(tomorrow, dashboardPage.getDueDate());
        assertEquals(high, dashboardPage.getPrio());
    }

    @Test
    //TODO case7
    public void verifyTitleIsRequired() {
        createToDoPage.clickCreate();
        assertTrue(createToDoPage.createToDoPageIsOpened(), "ToDo title should be required");
    }

    @Test
    //TODO case8
    public void verifyCancelButtonFunctionality() {
        createToDoPage.typeDueDate("today");
        createToDoPage.typeTitle(title);
        createToDoPage.selectPriority(high);
        createToDoPage.clickCancel();
        assertFalse(createToDoPage.deleteButtonIsDisplayed(), "Cancel button does not work");
        assertTrue(dashboardPage.DashboardIsOpened(), "Was not redirected to dashboard");
    }
}
