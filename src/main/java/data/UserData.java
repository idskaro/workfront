package data;

public class UserData {
    private String username;
    private String userPassword;
    private String userEmail;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }


    public static class UserDataBuilder {
        private UserData userData;

        private UserDataBuilder() {
            userData = new UserData();
        }

        public static UserDataBuilder userData() {
            return new UserDataBuilder();
        }

        public UserDataBuilder withUserName(String userName) {
            userData.username = userName;
            return this;
        }

        public UserDataBuilder withUserEmail(String userEmail) {
            userData.userEmail = userEmail;
            return this;
        }

        public UserDataBuilder withUserPassword(String userPassword) {
            userData.userPassword = userPassword;
            return this;
        }

        public UserData build() {
            return userData;
        }
    }

}
