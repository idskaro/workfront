package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DashboardPage extends BasePage {
    @FindBy(xpath = "//a[contains(.,'Create a todo')]")
    private WebElement createToDo;
    @FindBy(xpath = "//a[contains(.,' My account')]")
    private WebElement myAccount;
    @FindBy(xpath = "//a[contains(.,'Home')]")
    private WebElement home;
    @FindBy(xpath = "html/body/div[2]/div/div[2]/div/table/tbody/tr[1]/td[2]")
    private WebElement firstTitle;
    @FindBy(xpath = "html/body/div[2]/div/div[2]/div/table/tbody/tr[1]/td[3]")
    private WebElement firstDueDate;
    @FindBy(xpath = "html/body/div[2]/div/div[2]/div/table/tbody/tr[1]/td[4]")
    private WebElement firstPrio;

    public DashboardPage() {
    }

    public void clickCreateToDo() {
        click(createToDo);
    }

    public void clickMyAccount() {
        click(myAccount);
    }

    public void clickHome() {
        click(home);
    }

    public boolean isUserLogedIn() {
        return getCurrentURL().endsWith("/user/todos");
    }

    public void logOut() {
        visit(BASE_URL + "/user/logout");
    }

    public void visitDashboardPage() {
        visit(getUrl());
    }

    public String getDueDate() {
        return firstDueDate.getText();
    }

    public String getTitle() {
        return firstTitle.getText();
    }

    public String getPrio() {
        return firstPrio.getText().toLowerCase();
    }


    public boolean DashboardIsOpened() {
        return getCurrentURL().endsWith("/user/todos");
    }

    @Override
    public String getUrl() {
        return BASE_URL + "/user/todos";
    }
}
